use std::env;

/// Parses a list of command line arguments in format 1 2 3 ...n to add them to a vector of numbers
fn parse_command_line(args: Vec<String>) -> Vec<u16> {
    let mut nums: Vec<u16> = Vec::new();
    for arg in &args {
        nums.push(match arg.parse() {
            Ok(num) => {
                num
            }
            Err(_) => {
                panic!("Could not parse command line");
            }
        })
    }

    return nums;
}

#[test]
fn test_parse_command_line_success() {
    let args: Vec<String> = vec!["1".to_string(), "2".to_string(), "3".to_string()];
    assert_eq!(parse_command_line(args), vec![1, 2, 3]);
}

#[test]
#[should_panic]
fn test_parse_command_line_unable_to_parse() {
    let args: Vec<String> = vec!["1".to_string(), "2".to_string(), "k".to_string()];
    parse_command_line(args);
}

/// mutates the slice `orderable` and puts it in order of least to greatest
fn insertion_sort<T: Ord>(values: &mut [T]) {
    for i in 0..values.len() {
        for j in (0..i).rev() {
            if values[j] >= values[j + 1] {
                values.swap(j, j + 1);
            } else {
                break;
            }
        }
    }
}

#[test]
fn test_sorts_unordered() {
    let mut unordered_vector: Vec<u16> = vec![16, 8, 7, 1];
    insertion_sort_2(&mut unordered_vector);
    assert_eq!(unordered_vector, vec![1, 7, 8, 16]);
}

#[test]
fn test_sorts_ordered() {
    let mut unordered_vector: Vec<u16> = vec![1, 7, 8, 16];
    insertion_sort_2(&mut unordered_vector);
    assert_eq!(unordered_vector, vec![1, 7, 8, 16]);
}

fn main() {
    let args: Vec<String> = env::args().skip(1).collect();
    let mut nums_to_sort: Vec<u16> = parse_command_line(args);

    insertion_sort(&mut nums_to_sort);

    for &num in &nums_to_sort {
        println!("{}", num.to_string());
    }
}
